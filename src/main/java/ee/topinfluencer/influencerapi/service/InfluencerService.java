package ee.topinfluencer.influencerapi.service;


import ee.topinfluencer.influencerapi.model.Influencer;
import ee.topinfluencer.influencerapi.repository.InfluencerRepository;
import ee.topinfluencer.influencerapi.util.HTTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InfluencerService {


    @Autowired
    private InfluencerRepository influencerRepository;

    @Autowired
    private InfluencerService influencersService;


    public void updateInstagramFollowers(Influencer influencer) throws Exception {

        int followersCount = HTTPClient.getNewInstagramFollowers(influencer.getUser_link());

        influencerRepository.updateInstagramFollowers(influencer.getInfluencer_id(), followersCount);
    }

    public void updateInstagramInfluencers() throws Exception {

        List<Influencer> instagramInfluencers = influencerRepository.fetchInfluencer("Instagram");

        for (int i = 0; i < instagramInfluencers.size(); i++) {

            updateInstagramFollowers(instagramInfluencers.get(i));
            Thread.sleep(1000);
        }

    }

    public void updateYoutubeFollowers(Influencer influencer) throws Exception {

        int followersCount = HTTPClient.getNewYoutubeFollowers(influencer.getUser_link());

        influencerRepository.updateYoutubeFollowers(influencer.getInfluencer_id(), followersCount);
    }

    public void updateYoutubeInfluencers() throws Exception {

        List<Influencer> youtubeInfluencers = influencerRepository.fetchInfluencer("Youtube");

        for (int i = 0; i < youtubeInfluencers.size(); i++) {

            updateYoutubeFollowers(youtubeInfluencers.get(i));
            Thread.sleep(1000);
        }

    }

    //fixedRate = 5000
    //cron = "0 0 10 * * *"
    @Scheduled(fixedRate = 600000)
    public void updateAllFollowers() throws Exception {

        updateYoutubeInfluencers();
        updateInstagramInfluencers();
    }

    public static String getInstagramUsername(String user_link) {

        String[] choppedResponse = user_link.split("https://www.instagram.com/");

        String[] responseReady = choppedResponse[1].split("/");

        String userName = responseReady[0];

        return userName;
    }

}