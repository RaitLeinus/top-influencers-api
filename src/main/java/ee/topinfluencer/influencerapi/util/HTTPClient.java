package ee.topinfluencer.influencerapi.util;


import ee.topinfluencer.influencerapi.repository.InfluencerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HTTPClient {

    @Autowired
    private InfluencerRepository influencerRepository;

    public static int getNewInstagramFollowers(String user_link) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(user_link))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String content = response.body();

        try {
            String[] choppedResponse = content.split("userInteractionCount\":\"");
            String[] followerString = choppedResponse[1].split("\"}}");
            return Integer.parseInt(followerString[0]);
        }
        catch(Exception e) {
            String[] choppedResponse = content.split("<meta content=\"");
            String[] followerString = choppedResponse[1].split(" Followers,");

            if(!followerString[0].contains("k")) return Integer.parseInt(followerString[0].replaceAll("\\D*", ""));

            else{
                char[] newCharFollowers = followerString[0].toCharArray();

                if(newCharFollowers[2] == '.'){
                    String newFollowers = String.copyValueOf(newCharFollowers);
                    String fixedString = newFollowers.replace("k", "00");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));

                } else {
                    String newFollowers = String.copyValueOf(newCharFollowers);
                    String fixedString = newFollowers.replace("k", "000");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
                }
            }
        }

    }

    public static int getNewYoutubeFollowers(String user_link) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(user_link))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        String content = response.body();

        String[] choppedResponse = content.split("yt-subscription-button-subscriber-count-branded-horizontal subscribed yt-uix-tooltip\" title=\"");

        String[] followerString = choppedResponse[1].split("\" tabindex=\"0\" aria-label=\"");

        if(followerString[0].contains("mln")) return Integer.parseInt(followerString[0].replace("mln","0000").replaceAll("\\D*", ""));

        else
            if (!followerString[0].contains("tuh")) {

            return Integer.parseInt(followerString[0].replaceAll("\\D*", ""));

        } else {

            char[] newCharFollowers = followerString[0].toCharArray();

            if (newCharFollowers[1] == ',') {

                String newFollowers = String.copyValueOf(newCharFollowers);

                if (newFollowers.length() == 8) {

                    String fixedString = newFollowers.replace(" tuh", "0");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
                } else if (newFollowers.length() == 7) {

                    String fixedString = newFollowers.replace(" tuh", "00");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
                }

            } else if (newCharFollowers[2] == ',') {

                String newFollowers = String.copyValueOf(newCharFollowers);
                String fixedString = newFollowers.replace(" tuh", "00");
                return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
            } else {
                String newFollowers = String.copyValueOf(newCharFollowers);

                if (newFollowers.length() == 6 || newFollowers.length() == 7) {
                    String fixedString = newFollowers.replace(" tuh", "000");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
                } else if (newFollowers.length() == 5) {
                    String fixedString = newFollowers.replace(" tuh", "00");
                    return Integer.parseInt(fixedString.replaceAll("\\D*", ""));
                }

            }

        }
        return 0;
    }

    public static String getInstagramThumbnail(String user_link) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(user_link))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        String content = response.body();

        String[] choppedResponse = content.split("<meta property=\"og:image\" content=\"");

        String[] responseReady = choppedResponse[1].split("\"");

        return responseReady[0];
    }

    public static String getYoutubeThumbnail(String user_link) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(user_link))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        String content = response.body();

        String[] choppedResponse = content.split("<link rel=\"image_src\" href=\"");

        String[] responseReady = choppedResponse[1].split("\">");

        return responseReady[0];
    }

    public static String getYoutubeUsername(String user_link) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(user_link))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        String content = response.body();

        String[] choppedResponse = content.split("<meta name=\"title\" content=\"");

        String[] responseReady = choppedResponse[1].split("\">");

        return responseReady[0];
    }
}