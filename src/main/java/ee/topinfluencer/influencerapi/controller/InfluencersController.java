package ee.topinfluencer.influencerapi.controller;


import ee.topinfluencer.influencerapi.model.Influencer;
import ee.topinfluencer.influencerapi.repository.InfluencerRepository;
import ee.topinfluencer.influencerapi.service.InfluencerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/")
@CrossOrigin("*")
public class InfluencersController {


    @Autowired
    private InfluencerRepository influencerRepository;

    @Autowired
    private InfluencerService influencerService;


    @GetMapping("/influencers")
    public List<Influencer> getInfluencers() {
        return influencerRepository.fetchInfluencers();
    }

    @GetMapping("/influencersadmin")
    public List<Influencer> getInfluencersByAddedTime() {
        return influencerRepository.fetchInfluencersByAddedTime();
    }

    @GetMapping("/influencer")
    public List<Influencer> getInfluencer(@RequestParam("platform_name") String platform) {
        return influencerRepository.fetchInfluencer(platform);
    }

    @GetMapping("/influencerusername")
    public List<Influencer> getInfluencerUsername(@RequestParam("username") String username) {
        return influencerRepository.fetchInfluencerUsername(username);
    }


    @PostMapping("/add")
    public void addInfluencer(@RequestParam("user_link") String user_link) throws Exception {

        String[] choppedResponse = user_link.split("https://www.");

        String[] responseReady = choppedResponse[1].split(".com/");

        String platform = responseReady[0];

        if (platform.equals("instagram")) influencerRepository.addInstagramInflunecer(user_link);

        else if (platform.equals("youtube")) influencerRepository.addYoutubeInflunecer(user_link);

    }

    @DeleteMapping("/influencer/{id}")
    public void deleteInfluencer(@PathVariable("id") int id){
        influencerRepository.deleteInfluencer(id);
    }
}
