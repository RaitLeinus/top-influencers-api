package ee.topinfluencer.influencerapi.controller;

import ee.topinfluencer.influencerapi.dto.GenericResponseDto;
import ee.topinfluencer.influencerapi.dto.JwtRequestDto;
import ee.topinfluencer.influencerapi.dto.JwtResponseDto;
import ee.topinfluencer.influencerapi.dto.UserRegistrationDto;
import ee.topinfluencer.influencerapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}