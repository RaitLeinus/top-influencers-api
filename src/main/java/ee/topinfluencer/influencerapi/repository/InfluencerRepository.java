package ee.topinfluencer.influencerapi.repository;


import ee.topinfluencer.influencerapi.model.Influencer;
import ee.topinfluencer.influencerapi.service.InfluencerService;
import ee.topinfluencer.influencerapi.util.HTTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class InfluencerRepository {


    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Influencer> fetchInfluencers() {

        List<Influencer> influencers = jdbcTemplate.query(
                "select r.* from (select \n" +
                        "i.*,\n" +
                        "(\n" +
                        "select ih.followers from influencer_history ih where ih.influencer_id=i.influencer_id order by ih.date desc limit 1\n" +
                        ") as followers_count \n" +
                        "from influencer i) r order by r.followers_count desc",
                (row, rowNum) -> {
                    return new Influencer(
                            row.getInt("influencer_id"),
                            row.getString("username"),
                            row.getString("image"),
                            row.getString("platform_name"),
                            row.getInt("followers_count"),
                            row.getString("user_link"));
                });

        return influencers;

    }

    public List<Influencer> fetchInfluencersByAddedTime() {

        List<Influencer> influencers = jdbcTemplate.query(
                "select r.* from (select \n" +
                        "i.*,\n" +
                        "(\n" +
                        "select ih.followers from influencer_history ih where ih.influencer_id=i.influencer_id order by ih.date desc limit 1\n" +
                        ") as followers_count \n" +
                        "from influencer i) r order by r.influencer_id desc",
                (row, rowNum) -> {
                    return new Influencer(
                            row.getInt("influencer_id"),
                            row.getString("username"),
                            row.getString("image"),
                            row.getString("platform_name"),
                            row.getInt("followers_count"),
                            row.getString("user_link"));
                });

        return influencers;

    }

    public List<Influencer> fetchInfluencer(String platform) {

        List<Influencer> influencers = jdbcTemplate.query(
                "select r.* from(select \n" +
                        "i.*,\n" + "(\n" + "select ih.followers from influencer_history ih where ih.influencer_id=i.influencer_id order by ih.date desc limit 1\n" +
                        ") as followers_count \n" + "from influencer i WHERE platform_name = ?) r order by r.followers_count desc",
                new Object[]{platform},
                (row, rowNum) -> {
                    return new Influencer(
                            row.getInt("influencer_id"),
                            row.getString("username"),
                            row.getString("image"),
                            row.getString("platform_name"),
                            row.getInt("followers_count"),
                            row.getString("user_link"));
                });
        return influencers;
    }

    public List<Influencer> fetchInfluencerUsername(String username) {

        List<Influencer> influencers = jdbcTemplate.query(
                "select r.* from(select \n" +
                        "i.*,\n" + "(\n" + "select ih.followers from influencer_history ih where ih.influencer_id=i.influencer_id order by ih.date desc limit 1\n" +
                        ") as followers_count \n" + "from influencer i WHERE username = ?) r order by r.followers_count desc",
                new Object[]{username},
                (row, rowNum) -> {
                    return new Influencer(
                            row.getInt("influencer_id"),
                            row.getString("username"),
                            row.getString("image"),
                            row.getString("platform_name"),
                            row.getInt("followers_count"),
                            row.getString("user_link"));
                });
        return influencers;
    }

    public void updateInstagramFollowers(int influencer_id, int influencer_count) throws Exception {
        jdbcTemplate.update("INSERT INTO influencer_history (influencer_id, followers) VALUES (?, ?)", influencer_id, influencer_count);
    }

    public void updateYoutubeFollowers(int influencer_id, int influencer_count) throws Exception {
        jdbcTemplate.update("INSERT INTO influencer_history (influencer_id, followers) VALUES (?, ?)", influencer_id, influencer_count);
    }

    public void addInstagramInflunecer(String user_link) {
        try {
            String userName = InfluencerService.getInstagramUsername(user_link);
            String image = HTTPClient.getInstagramThumbnail(user_link);
            String platformName = "Instagram";

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(
                                        "INSERT INTO influencer (username, image, platform_name, user_link) VALUES (?,?,?,?)",
                                        Statement.RETURN_GENERATED_KEYS
                                );
                        ps.setString(1, userName);
                        ps.setString(2, image);
                        ps.setString(3, platformName);
                        ps.setString(4, user_link);
                        return ps;
                    },
                    keyHolder
            );
            updateInstagramFollowers(keyHolder.getKey().intValue(), HTTPClient.getNewInstagramFollowers(user_link));
        } catch (Exception e) {
            System.out.println("Error occurred: " + e.toString());
        }

    }

    public void addYoutubeInflunecer(String user_link) throws Exception {
        String userName = HTTPClient.getYoutubeUsername(user_link);
        String image = HTTPClient.getYoutubeThumbnail(user_link);
        String platformName = "Youtube";
        String userLink = user_link;

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    "INSERT INTO influencer (username, image, platform_name, user_link) VALUES (?,?,?,?)",
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setString(1, userName);
                    ps.setString(2, image);
                    ps.setString(3, platformName);
                    ps.setString(4, userLink);
                    return ps;
                },
                keyHolder
        );
        updateYoutubeFollowers(keyHolder.getKey().intValue(), HTTPClient.getNewYoutubeFollowers(user_link));
    }


    public void deleteInfluencer(int influencerID) {
        jdbcTemplate.update("DELETE FROM influencer_history WHERE influencer_id = ?", influencerID);
        jdbcTemplate.update("DELETE FROM influencer WHERE influencer_id = ?", influencerID);

    }
}
