package ee.topinfluencer.influencerapi.repository;

import ee.topinfluencer.influencerapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(iduser) from user where user_account = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`user_account`, `password`) values (?, ?)", user.getUsername(), user.getPassword());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `user_account` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("iduser"), rs.getString("user_account"), rs.getString("password"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }
}
