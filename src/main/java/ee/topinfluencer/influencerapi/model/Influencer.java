package ee.topinfluencer.influencerapi.model;

public class Influencer {

    private int influencer_id;
    private String username;
    private String image;
    private String platform_name;
    private int followers_count;
    private String user_link;


    public Influencer() {
    }

    public Influencer(int influencer_id, String username, String image, String platform_name, int followers_count, String user_link) {
        this.influencer_id = influencer_id;
        this.username = username;
        this.image = image;
        this.platform_name = platform_name;
        this.followers_count = followers_count;
        this.user_link = user_link;
    }

    public int getInfluencer_id() {
        return influencer_id;
    }

    public void setInfluencer_id(int influencer_id) {
        this.influencer_id = influencer_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPlatform_name() {
        return platform_name;
    }

    public void setPlatform_name(String platform_name) {
        this.platform_name = platform_name;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public String getUser_link() {
        return user_link;
    }

    public void setUser_link(String user_link) {
        this.user_link = user_link;
    }
}
